# UW Workday Studio Plugins

## Description

This project contains Workday Studio plugins developed by the UW. The full source for these plugins can be found at the repository URL below.

https://git.doit.wisc.edu/atp/integrations/helper-scripts

The plugins can be installed using the following URLs in Workday Studio.

Assembly Formatter:
http://uw-workday-studio-plugins-jason-deschard-cca8f9624a9011eeac4b89.pages.doit.wisc.edu/Studio_Assembly_Formatter_Plugin/

Studio Validator:
http://uw-workday-studio-plugins-jason-deschard-cca8f9624a9011eeac4b89.pages.doit.wisc.edu/Studio_Validator_Plugin/